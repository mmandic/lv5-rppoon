using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> list = new List<IShipable>();
            Product laptop = new Product("Acer", 58, 20);
            Product mobilePhone = new Product("SamsungJ5", 10, 8);
            Product monitor = new Product("NOC", 10, 10);
            Box desk = new Box("Desk");
            desk.Add(laptop);
            desk.Add(mobilePhone);
            desk.Add(monitor);
            list.Add(desk);

            Product chair = new Product("White chair", 3, 4);
            Product table = new Product("Table", 5, 6);
            Box kitchen = new Box("Kitchen");
            kitchen.Add(chair);
            kitchen.Add(table);
            list.Add(kitchen);


            foreach (IShipable item in list)
                Console.WriteLine(item.Description());
            
        }
    }
}
