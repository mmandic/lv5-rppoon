using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON
{
    class ShippingService
    {
        private double pricePerKilogram; 

        public ShippingService()
        {
            this.pricePerKilogram = 1.5;
        }

        public ShippingService(double pricePerKilogram)
        {
            this.pricePerKilogram = pricePerKilogram;
        }

        public double GetShippingServicePrice(IShipable package)
        {
            return package.Weight * pricePerKilogram;
        }
    }
}
