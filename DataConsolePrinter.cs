using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace LV5_rppoon_zad3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void PrintData(IDataset data)
        {
            ReadOnlyCollection<List<string>> Data = data.GetData();
            if (Data == null)
            {
                Console.WriteLine("No data");
                return;
            }
            foreach (List<string> lines in Data)
            {
                foreach (string line in lines)
                {
                    Console.Write(line + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
