using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace LV5_rppoon_zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("file.txt");
            User user1 = User.GenerateUser("Scarlett");
            User user2 = User.GenerateUser("Audrey");
            User user3 = User.GenerateUser("Angelina");
            User user4 = User.GenerateUser("Marilyn");
            
            
            ProtectionProxyDataset protectionProxyDataset1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset protectionProxyDataset2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset protectionProxyDataset3 = new ProtectionProxyDataset(user3);
            ProtectionProxyDataset protectionProxyDataset4 = new ProtectionProxyDataset(user4);
            
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("file.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.PrintData(protectionProxyDataset1);
            Console.WriteLine();
            printer.PrintData(protectionProxyDataset2);
            Console.WriteLine();
            printer.PrintData(protectionProxyDataset3);
            Console.WriteLine();
            printer.PrintData(protectionProxyDataset4);
            Console.WriteLine();
            
            Console.WriteLine("Virtual: \n");
            printer.PrintData(virtualProxyDataset);
        }
    }
}
