using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace LV5_rppoon_zad3
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
